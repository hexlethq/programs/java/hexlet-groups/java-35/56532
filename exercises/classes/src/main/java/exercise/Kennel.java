package exercise;

import java.util.Arrays;

// BEGIN
class Kennel {
    private static String[][] puppies = new String[100][2];
    private static int count = 0;
    private static final int NAME = 0;
    private static final int BREED = 1;

    public static void addPuppy(String[] puppy) {
        puppies[count++] = puppy.clone();
    }
    
    public static void addSomePuppies(String[][] puppies) {
        for (String[] puppy : puppies) {
            Kennel.addPuppy(puppy);
        }
    }
    
    public static int getPuppyCount() {
        return count;
    }
    
    public static boolean isContainPuppy(String name) {
        for (int i = 0; i < count; i++) {
            if (name.equals(puppies[i][NAME])) {
                return true;
            }
        }
        return false;
    }
    
    public static String[][] getAllPuppies() {
        return Arrays.copyOfRange(puppies, 0, count);
    }
    
    public static String[] getNamesByBreed(String breed) {
        int size = 0;

        for (int i = 0; i < count; i++) {
            if (breed.equals(puppies[i][BREED])) {
                size++;
            }
        }

        String[] names = new String[size];
        int counter = 0;

        for (int i = 0; i < count; i++) {
            if (breed.equals(puppies[i][BREED])) {
                names[counter++] = puppies[i][NAME];
            }
        }
        return names;
    }
    
    public static void resetKennel() {
        count = 0;
        Arrays.fill(puppies, null);
    }

    public static boolean removePuppy(String name) {
        for (int i = 0; i < count; i++) {
            if (name.equals(puppies[i][NAME])) {
                for (int j = i; j < count - 1; j++) {
                    puppies[j] = puppies[j + 1];
                }
                count--;
                return true;
            }
        }
        return false;
    }
}
// END
