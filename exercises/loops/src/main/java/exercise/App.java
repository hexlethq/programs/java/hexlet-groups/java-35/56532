package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String text) {
        StringBuilder result = new StringBuilder();
        text = text.trim().replaceAll(" +", " ");

        for (int i = 0; i < text.length(); i++) {
            if (i == 0) {
                result.append(text.charAt(0));
            }
            if (text.charAt(i) == ' ' && i != text.length() - 1) {
                result.append(text.charAt(i + 1));
            }
        }
        return result.toString().toUpperCase();
    }
    // END
}
