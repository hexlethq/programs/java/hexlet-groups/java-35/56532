package exercise;

import java.lang.reflect.Array;
import java.util.Arrays;

class Point {
    final static int X = 0;
    final static int Y = 1;

    public static int[] makePoint(int x, int y) {
        return new int[]{x, y};
    }

    public static int getX(int[] point) {
        return point[X];
    }

    public static int getY(int[] point) {
        return point[Y];
    }

    public static String pointToString(int[] point)
    {
        return Arrays.toString(point).
            replace('[', '(').
            replace(']', ')');
    }

    public static int getQuadrant(int[] point)
    {
        if (point[X] > 0 && point[Y] > 0) {
            return 1;
        } else if (point[X] < 0 && point[Y] > 0) {
            return 2;
        } else  if (point[X] < 0 && point[Y] < 0) {
            return 3;
        } else if (point[X] > 0 && point[Y] < 0) {
            return 4;
        } else {
            return 0;
        }
    }

    public static int[] getSymmetricalPointByX(int[] point)
    {
        if (point[Y] == 0) {
            return point.clone();
        }
        return new int[]{point[X], -point[Y]};
    }

    public static double calculateDistance(int[] pointA, int[] pointB)
    {
        return Math.sqrt(Math.pow(pointB[X] - pointA[X], 2) + Math.pow(pointB[Y] - pointA[Y], 2));
    }

    public static void main(String[] args) {
        int[] point1 = makePoint(0, 0);
        int[] point2 = makePoint(3, 4);
        double r = Point.calculateDistance(point1, point2); // 5
        System.out.println(r);
    }
}
