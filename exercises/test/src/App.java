import java.util.Arrays;
import commons.langs.ArrayUtils

public class App {
    public static int[] swap(int[] arr) {
        int[] result = arr.clone();
        if (result.length > 1) {
            int tmp = result[0];
            result[0] = result[result.length - 1];
            result[result.length - 1] = tmp;
        }
        return result;
    }

    public static void main(String[] args) {
    }
}
