package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(double a, double b, double ab)
    {
        return a * b / 2 * Math.sin(ab * Math.PI / 180);
    }

    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
