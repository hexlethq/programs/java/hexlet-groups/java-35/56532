package exercise;

class Converter {
    // BEGIN
    public static int convert(int count, String units)
    {
        int bytesInKByte = 1024;

        return switch (units) {
            case "b" -> count * bytesInKByte;
            case "Kb" -> count / bytesInKByte;
            default -> 0;
        };
    }

    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
