package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = number > 1000 && number % 2 != 0;
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        System.out.println(number % 2 == 0 ? "yes" : "no");
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        switch (minutes / 15) {
            case 0 -> System.out.println("First");
            case 1 -> System.out.println("Second");
            case 2 -> System.out.println("Third");
            case 3 -> System.out.println("Fourth");
        }
        // END
    }
}
