package exercise;

import java.util.Locale;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        String out = sentence.charAt(sentence.length() - 1) == '!' ?
            sentence.toUpperCase() :
            sentence.toLowerCase();

        System.out.println(out);
        // END
    }
}
