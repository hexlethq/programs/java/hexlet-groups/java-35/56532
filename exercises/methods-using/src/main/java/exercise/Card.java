package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        int tail = 4;
        int width = starsCount + tail;

        String result = String.format("%" + width + "s", cardNumber.substring(cardNumber.length() - tail))
            .replace(' ', '*');

        System.out.println(result);
        // END
    }
}
