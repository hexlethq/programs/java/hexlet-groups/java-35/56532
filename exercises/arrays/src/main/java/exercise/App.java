package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] arr)
    {
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[arr.length - i - 1];
        }
        return result;
    }

    public static int mult(int[] arr)
    {
        int result = 1;

        for (int el: arr) {
            result *= el;
        }
        return result;
    }

    public static int[] flattenMatrix(int[][] matrix) {
        int size = 0;
        int pointer = 0;

        for (int[] vector : matrix) {
            size += vector.length;
        }

        int[] result = new int[size];

        for (int[] vector : matrix) {
            System.arraycopy(vector, 0, result, pointer, vector.length);
            pointer += vector.length;
        }
        return result;
    }
    // END
}
