// BEGIN
package exercise.geometry;

public class Segment
{
    public static double[][] makeSegment(double[] a, double[] b)
    {
         double[][] segment = new double[2][2];
         segment[0] = a.clone();
         segment[1] = b.clone();
         return segment;
    }

    public static double[] getBeginPoint(double[][] segment)
    {
        return segment[0];
    }

    public static double[] getEndPoint(double[][] segment)
    {
        return segment[1];
    }
}
// END
