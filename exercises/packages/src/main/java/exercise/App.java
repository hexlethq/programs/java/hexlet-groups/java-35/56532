// BEGIN
package exercise;

import exercise.geometry.Point;
import exercise.geometry.Segment;

class App
{
    public static double[] getMidpointOfSegment(double[][] segment)
    {
        double[] middle = new double[2];
        middle[0] = (segment[0][0] + segment[1][0]) / 2;
        middle[1] = (segment[0][1] + segment[1][1]) / 2;
        return middle;
    }

    public static double[][] reverse(double[][] segment)
    {
        double[][] reversed = new double[2][2];
        reversed[0] = segment[1].clone();
        reversed[1] = segment[0].clone();
        return reversed;
    }

    public static boolean isBelongToOneQuadrant(double[][] segment)
    {
        return segment[0][0] * segment[1][0] > 0 && segment[0][1] * segment[1][1] > 0;
    }

    public static void main(String[] args)
    {
        double[][] segment1 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(5, 8));
        System.out.println(App.isBelongToOneQuadrant(segment1));

        double[][] segment2 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(-2, 8));
        System.out.println(App.isBelongToOneQuadrant(segment2));

        double[][] segment3 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(0, 0));
        System.out.println(App.isBelongToOneQuadrant(segment3));
    }
}
// END
