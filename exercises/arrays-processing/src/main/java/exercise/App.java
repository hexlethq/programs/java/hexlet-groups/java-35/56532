package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr)
    {
        int maxNegative = Integer.MIN_VALUE;
        int index = -1;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && arr[i] > maxNegative) {
                maxNegative = arr[i];
                index = i;
            }
        }
        return index;
    }

    public static int[] getElementsLessAverage(int[] arr)
    {
        if (arr.length == 0) {
            return arr;
        }

        int sum = 0, size = 0, pointer = 0;

        for (int el: arr) {
            sum += el;
        }

        int average = sum / arr.length;

        for (int el: arr) {
            if (el <= average) {
                size++;
            }
        }

        int[] result = new int[size];

        for (int el : arr) {
            if (el <= average) {
                result[pointer++] = el;
            }
        }
        return result;
    }

    public static int getSumBetweenMinAndMax(int[] arr)
    {
        int sum = 0;
        int minIndex = 0;
        int maxIndex = 0;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[minIndex]) {
                minIndex = i;
            }
            if (arr[i] > arr[maxIndex]) {
                maxIndex = i;
            }
        }

        int[] slice = minIndex > maxIndex ?
            Arrays.copyOfRange(arr, maxIndex + 1, minIndex) :
            Arrays.copyOfRange(arr, minIndex + 1, maxIndex);

        for (int el : slice) {
            sum += el;
        }
        return sum;
    }
    // END
}
