package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c)
    {
        if (a + b > c && b + c > a && a + c > b) {
            if (a == b && b == c) {
                return "Равносторонний";
            } else if (a == b || b == c || a == c) {
                return "Равнобедренный";
            } else {
                return "Разносторонний";
            }
        }
        return "Треугольник не существует";
    }
    // END
}
