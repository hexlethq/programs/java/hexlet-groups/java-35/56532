package exercise;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] arr)
    {
        if (arr.length == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        sb.append("<ul>\n");
        for (String el : arr) {
            sb.append("  <li>");
            sb.append(el);
            sb.append("</li>\n");
        }
        sb.append("</ul>");

        return sb.toString();
    }

    public static String getUsersByYear(String[][] users, int year)
    {
        StringBuilder sb = new StringBuilder();

        for (String[] user : users) {
            LocalDate dob = LocalDate.parse(user[1], DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            if (dob.getYear() == year) {
                sb.append("  <li>");
                sb.append(user[0]);
                sb.append("</li>\n");
            }
        }

        if (sb.length() > 0) {
            sb.insert(0, "<ul>\n");
            sb.append("</ul>");
        }

        return sb.toString();
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) {
        // BEGIN
        String[] youngest = null;

        LocalDate targetDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH));

        for (String[] user : users) {
            LocalDate dob = LocalDate.parse(user[1], DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            if (dob.isBefore(targetDate)) {
                if (youngest == null) {
                    youngest = user.clone();
                } else {
                    if (dob.isAfter(LocalDate.parse(youngest[1], DateTimeFormatter.ofPattern("yyyy-MM-dd")))) {
                        youngest = user.clone();
                    }
                }
            }
        }
        return  youngest == null ? "" : youngest[0];
        // END
    }

    public static void main(String[] args) {
        String[][] users = {
                {"Andrey Petrov", "1990-11-23"},
                {"Aleksey Ivanov", "2000-02-15"},
                {"Anna Sidorova", "1996-09-09"},
                {"John Smith", "1989-03-11"},
                {"Vanessa Vulf", "1985-11-16"},
                {"Vladimir Nikolaev", "1990-12-27"},
                {"Alice Lucas", "1986-01-01"},
                {"Elsa Oscar", "1989-03-10"},
        };
        String res = getYoungestUser(users, "11 Jul 1989"); // "John Smith"
        System.out.println(res);
    }
}
