package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] arr)
    {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        return arr;
    }

    public static int[] insertsSort(int[] arr)
    {
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;

            for (int j = i; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }

            if (minIndex != i) {
                int tmp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = tmp;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        int[] numbers = {3, 10, 4, 3};
        int[] sorted = App.insertsSort(numbers);
        System.out.println(Arrays.toString(sorted)); // => [3, 3, 4, 10]
    }
    // END
}
